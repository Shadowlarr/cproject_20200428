#include <iostream>

using namespace std;


template <typename DataStack>
struct Data
{
	Data<DataStack>* Previous;
	DataStack Value;
};

template <typename DataStack>
class Stack
{
private:
	Data<DataStack>* Top = nullptr;
	Data<DataStack>* Current_elem = nullptr;
public:
	void Push(DataStack q)
	{
		Current_elem = new Data<DataStack>;
		Current_elem->Value = q;
		Current_elem->Previous = Top;
		Top = Current_elem;
	}

	DataStack Pop()
	{
		if (Top != nullptr)
		{
			DataStack q = Top->Value;
			Current_elem = Top;			// ��������� ������ ���...	(� �� ������, ��� ��� ���������, �� �����-�� ������ �������� ��������� �� ���������)
			Top = Top->Previous;
			delete Current_elem;		// ...���...
			Current_elem = nullptr;		// ...� ���
			return q;
		}
		else
		{
			cout << "Error" << '\n';
			return -1;
		}
	}
};


int main()
{
	Stack<char> Test;
	char a = 0;
	int b = 0;

	cout << "Enter the amount of items:" << '\n';
	cin >> b;

	for (int i = 0; i < b; ++i)
	{
		cout << "Enter Data:" << '\n';
		cin >> a;
		Test.Push(a);
	}

	cout << "Data output:" << '\n';
	for (int i = 0; i < b; ++i)
	{
		cout << Test.Pop() << ' ';
	}

	return 0;
}